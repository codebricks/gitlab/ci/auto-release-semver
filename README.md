[TOC]

[semver]: https://semver.org
[semver_v123]: https://semver.org/#is-v123-a-semantic-version
[semver_initial_release]: https://semver.org/#how-should-i-deal-with-revisions-in-the-0yz-initial-development-phase
[gitlab_releases]: https://docs.gitlab.com/ee/user/project/releases/
[gitlab_predefined_variables]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
[trunk_based_development]: https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development
[regexp_branch]: https://regex101.com/r/Mm8tmO/4
[regexp_tag]: https://regex101.com/r/8rSQPo/2

# auto-release-semver - auto creation gitlab release and tag after merge

## Short description

1. This pipeline template developed to automatically create [gitlab release][gitlab_releases] (and tags) after merge to default branch
1. It is based on [trunk-based development git flow][trunk_based_development]
1. There is 3 jobs:
    - auto_release_semver_check - checking the tag and branch for compliance with regexp
    - auto_release_semver_prepare - preparing a version file (artifact) to create a release and tag
    - auto_release_semver_create - creating [gitlab release][gitlab_releases] (and tag)
1. The tag format strictly corresponds to [SemVer(Semantic Versioning)][semver] 
    - 1.2.3 - only numbers in versions supported
    - **v**1.2.3 - letters before version [**is not supported**][semver_v123]
        - next version will be without letters:
          - patch - 1.2.4
          - minor - 1.3.0
          - major - 2.0.0
    - [tag regexp condition][regexp_tag]
1. Pipeline implies the use of branch names starting with `major/`, `minor/` and `patch/`
    - [branch regexp condition][regexp_branch]
1. Necessary repository settings:
    - Push to default branch must be prohibited
        - Settings => Repository => Protected branches => Allowed to push = "No one"
        - This is necessary to avoid accidental push to the default branch and, as a result, the launch auto_release_semver_prepare and auto_release_semver_create jobs
    - Merge method must be `Merge commit` or `Merge commit with semi-linear history`
      - auto_release_semver_prepare use information about source branch from merge commit to create an artifact with tag

## Git graph

```mermaid
gitGraph TB:
  commit id: "initial commit"
  branch minor/init-release
  commit
  checkout main
  merge minor/init-release tag: "0.1.0"
  branch minor/feature2
  commit
  checkout main
  branch patch/init-release-fix
  commit
  checkout main
  merge patch/init-release-fix tag: "0.1.1"
  checkout minor/feature2
  commit
  merge main
  commit
  checkout main
  merge minor/feature2 tag: "0.2.0"
  branch minor/feature3
  commit
  checkout main
  branch patch/feature2-fix
  commit
  checkout main
  branch major/stable-release
  commit
  checkout main
  merge major/stable-release tag: "1.0.0"
  checkout patch/feature2-fix
  commit
  checkout minor/feature3
  commit
  merge main
  checkout main
  merge minor/feature3 tag: "1.1.0"
  checkout patch/feature2-fix
  commit
  merge main
  checkout main
  merge patch/feature2-fix tag: "1.1.1"
```

## Logic

### Step 1 - auto_release_semver_check job

1. There is no specific rules for this job (it always starts in any branch, if pipeline is not filtered by workflow rules)
1. The job checks:
    - [tag regexp condition][regexp_tag]
    - [branch regexp condition][regexp_branch]
1. latest tag example:
    - **0.1.0**
    - **1.2.3**
    - **2022.4.1**
1. branch name example:
    - **patch/fix_name**
    - **minor/feature_name**
    - **major/incompatible_release_name**

### Step 2 - auto_release_semver_prepare

1. There is 3 rules, depend on [gitlab predefined variables][gitlab_predefined_variables]:

    | rule | explanation |
    | ---- | ----------- |
    | $CI_PIPELINE_SOURCE == "push" | works only if pipeline started with push (merge also satisfies this rule) |
    | $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH | push must be in $CI_DEFAULT_BRANCH |
    | $CI_COMMIT_TITLE =~ /(major\|minor\|patch)\/[a-zA-Z]+[a-zA-Z0-9\/_-]*[a-zA-Z0-9]+/ | version detection based on merge commit with a branch name |

1. The job create artifact with next version based on `major`, `minor` or `patch` word in merge commit comment (comment contains merged branch name)
    - if there were no tags in the repository, the increment will be for version 0.0.0
        - [first (initial) release should be minor][semver_initial_release]

1. Push to default branch must be prohibited


### Step 3 - auto_release_semver_create

1. There are the same rules as in auto_release_semver_prepare

1. The job create release and tag according to version in artifact from job auto_release_semver_prepare. Examples:

    | last tag | branch name prefix | next release and tag |
    | -------- | ------------------ | -------------------- |
    | no tags  | patch              | 0.0.1 ([don't do that][semver_initial_release])|
    | no tags  | minor              | 0.1.0                |
    | no tags  | major              | 1.0.0                |
    | 0.1.0    | patch              | 0.1.1                |
    | 0.1.0    | minor              | 0.2.0                |
    | 0.1.0    | major              | 1.0.0                |
    | v1.2.3   | patch              | 1.2.4                |
    | v1.2.3   | minor              | 1.3.0                |
    | v1.2.3   | major              | 2.0.0                |
